#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <string>
#include <iostream>
#include "personservice.h"
#include "displaysettings.h"

class ConsoleUI
{
public:
    ConsoleUI();
    void start();
    void seeCommands();
private:
    PersonService personService;
    void addPerson();
    void searchPerson(char &sCommand, string &sInp);
    void deletePerson();
    void lines();
    void display(vector<Person> personlist, DisplaySettings current);
    void displaysettings(DisplaySettings& current);
    void welcomeLine();
    bool check2options(char input, char option1, char option2);
    bool check5options(char input, char option1, char option2, char option3, char option4, char option5);    
};

#endif // CONSOLEUI_H
