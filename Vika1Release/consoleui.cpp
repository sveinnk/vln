#include "consoleui.h"
#include <iomanip>
#include <vector>
#include <cctype>

using namespace std;

ConsoleUI::ConsoleUI()
{
    personService = PersonService();
}

void ConsoleUI::start() {
    personService.getFile();
    bool checker = true;
    DisplaySettings current;
    string inp;
    welcomeLine();
    seeCommands();
    cout << endl;

    while(checker){

        cout << endl <<"Insert command: \n";
        cout << "> ";
        cin >> inp;
        cout << endl;


        if(inp == "add") {
            addPerson();

        }else if(inp == "display"){
            vector<Person> personlist = personService.getlist();
            display(personlist, current);

        }else if(inp == "search"){
            char sCommand;
            string sInp;

            searchPerson(sCommand, sInp);

            display(personService.search(sCommand, sInp), current);

        }else if(inp == "delete"){
            deletePerson();

        }else if(inp == "quit"){
            checker = false;

        }else if(inp == "settings"){
            displaysettings(current);

        }else if(inp == "commands"){
            seeCommands();

        }else{
            cout << "*Invalid input*\n\n";
        }
    }
}

void ConsoleUI::addPerson()
{
    Person p = Person();

    cout << "First name: "; cin >> p.firstName;
    cout << "Last name: "; cin >> p.lastName;
    cout << "Gender(M/F): "; cin >> p.gender;
    cout << "Year of birth: "; cin >> p.yearOfBirth;
    cout << "Year of death: "; cin >> p.yearOfDeath;

    p.firstName[0] = toupper(p.firstName[0]);
    p.lastName[0] = toupper(p.lastName[0]);
    p.gender[0] = toupper(p.gender[0]);

    personService.add(p);
    cout << endl;
}

void ConsoleUI::searchPerson(char& sCommand, string& sInp)
{
    bool checker = true;

    cout << "Search by:\n";
    cout << "- First Name(F)\n" << "- Last Name(L)\n" << "- Gender(G)\n" << "- Year Of Birth(B)\n" << "- Year Of Death(D)\n";

    while(checker){
        cout <<"> ";
        cin >> sCommand;
        sCommand = toupper(sCommand);
        checker = check5options(sCommand, 'F', 'L', 'G', 'B', 'D');
    }

    switch(sCommand){
        case 'F':
            cout << "Enter a first name to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'L':
            cout << "Enter a last name to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'G':
            cout << "Enter a gender to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'B':
            cout << "Enter a date to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'D':
            cout << "Enter a date to search for: ";
            cin >> sInp;
            checker = false;
            break;
        default:
            break;
        }

    cout << endl;
}

void ConsoleUI::deletePerson()
{
    Person p = Person();
    cout << "Insert: First name and last name.\n";
    cout << "> ";
    cin >> p.firstName >> p.lastName;
    personService.deletePerson(p);
    cout << endl;
}

void ConsoleUI::display(vector<Person> personlist, DisplaySettings current)
{

    personlist = personService.sortList(personlist, current.sortByChar);

    cout << setw(10) << "First Name" << setw(15) << "Last Name" << setw(12) << "Gender";
    cout << setw(20) << "Year Of Birth" << setw(20) << "Year Of Death\n";
    lines();
    if(current.sortTypeBool || (personlist.size() == 0)){
        for(unsigned int i = 0; i < personlist.size(); i++){
            cout << personlist[i];
        }
    }else{
        for(int i = (personlist.size() - 1); i >= 0; i--){
            cout << personlist[i];
        }
    }
    lines();
}

bool ConsoleUI::check2options(char input, char option1, char option2)
{
    bool checker = true;

    if(input == option1 || input == option2){
        checker = false;
    }else{
        cout << "*Invalid input*" << endl;
    }

    return checker;
}

bool ConsoleUI::check5options(char input, char option1, char option2, char option3, char option4, char option5)
{
    bool checker = true;

    if(input == option1 || input == option2 || input == option3 || input == option4 || input == option5){
        checker = false;
    }else{
        cout << "*Invalid input*" << endl;
    }

    return checker;
}

void ConsoleUI::lines()
{
    for(int k = 0; k < 80 ; k++){
        cout << "-";
    }
    cout << endl;
}

void ConsoleUI::displaysettings(DisplaySettings& current)
{
    char dCommand;
    char dGet;
    bool checker = true;
    char ans;

    cout << "Current display settings: ";
    cout << "Sorted by: " << current.sortBy << " - " << current.sortType << endl << endl;
    cout << "Do you want to change the settings?(Y/N)\n";

    while(checker){
        cin >> ans;
        ans = toupper(ans);
        checker = check2options(ans, 'Y', 'N');
    }

    checker = true;

    if(ans == 'Y'){
        cout << endl <<"Display based on:\n";
        cout << "- First Name(F)\n" << "- Last Name(L)\n" << "- Gender(G)\n" << "- Year Of Birth(B)\n" << "- Year Of Death(D)\n";

        while(checker){
            cin >> dCommand;
            dCommand = toupper(dCommand);

            checker = check5options(dCommand, 'F', 'L', 'G', 'B', 'D');
        }

        cout << endl;

        current.sortByChar = dCommand;

        checker = true;

        if(dCommand == 'F' || dCommand == 'L'){
            cout << "Alphabetical(A) or Reverse Alphabetical(R) order?\n";
            while(checker){
                cin >> dGet;
                dGet = toupper(dGet);
                checker = check2options(dGet, 'A', 'R');
            }
            if(dGet == 'A'){
                current.sortTypeBool = true;
            }else{
                current.sortTypeBool = false;
            }

            checker = true;

        }else if(dCommand == 'G'){
            cout << "Males first(M) or Females first(F)?\n";
            while(checker){
                cin >> dGet;
                dGet = toupper(dGet);
                checker = check2options(dGet, 'M', 'F');
            }
            if(dGet == 'M'){
                current.sortTypeBool = true;
            }else{
                current.sortTypeBool = false;
            }
            checker = true;

        }else{
            cout << "Increasing(I) or Decreasing(D) order?\n";
            while(checker){
                cin >> dGet;
                dGet = toupper(dGet);
                checker = check2options(dGet, 'I', 'D');
            }
            if(dGet == 'I'){
                current.sortTypeBool = true;
            }else{
                current.sortTypeBool = false;
            }
        }

        current.setSort();
    }
}

void ConsoleUI::seeCommands()
{
    cout << "The commands are: \n" << "- add\n" << "- delete\n" << "- display\n" << "- search\n" << "- settings\n" << "- commands\n" << "- quit\n";
}

void ConsoleUI::welcomeLine(){
    cout << setw(24) << "";
    for(int k = 0; k < 25 ; k++){
        cout << "*";
    }
    cout << endl;
    cout << setw(25)<<" ~~~~~~~~~~~~~~~~~~~~~~ " << "Welcome to the Database" << " ~~~~~~~~~~~~~~~~~~~~~~ " << endl ;
    cout << setw(24) << "";
    for(int k = 0; k < 25 ; k++){
        cout << "*";
    }
    cout << endl;
}
