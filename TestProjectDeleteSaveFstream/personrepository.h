#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include <vector>
#include "person.h"

using namespace std;

class PersonRepository
{
private:
    vector<Person> personList;
public:
    PersonRepository();
    void add(Person p);
    vector<Person> getlist();
    vector<Person> search(char sCommand, string sInp);
    void save();
    void deletePerson(Person p);
    void getFile();
};

#endif // PERSONREPOSITORY_H
