#include "personrepository.h"
#include "consoleui.h"
#include <fstream>
#include <iostream>
#include <vector>

PersonRepository::PersonRepository()
{
    vector<Person>personList;
}

void PersonRepository::add(Person p)
{
    personList.push_back(p);
    save();
}

vector<Person> PersonRepository::getlist()
{
    return personList;
}

vector<Person> PersonRepository::search(char sCommand, string sInp)
{
    vector<Person> searchList;

    if(personList.size() > 0){
        switch(sCommand){
            case 'F':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp == personList[i].firstName){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            case 'L':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp == personList[i].lastName){
                        searchList.push_back(personList[i]);
                    }
                }
            break;
            case 'B':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp == personList[i].yearOfBirth){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            case 'D':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp == personList[i].yearOfDeath){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            default:
                break;
        }
    }

    return searchList;

}

void PersonRepository::getFile()
{
    string word;
    Person p;
    int x = 0;
    ifstream inFile ("gagnagrunnur.txt");
    if (inFile.is_open())
    {
        while (getline(inFile,word,' '))
        {
            if(x==0){
                if(word[0] == '\n')
                {
                    word.erase(0,1);
                }
                p.firstName = word;
                x++;
            }
            else if(x==1){
                p.lastName = word;
                x++;
            }
            else if(x==2){
                p.gender = word;
                x++;
            }
            else if(x==3){
                p.yearOfBirth = word;
                x++;
            }
            else if(x==4){
                p.yearOfDeath = word;
                personList.push_back(p);
                x=0;
            }
        }
        inFile.close();
    }
}

void PersonRepository::save()
{
    ofstream outFile ("gagnagrunnur.txt");
    if(outFile.is_open()) {
        for(unsigned int x = 0; x < personList.size(); x++)
        {
            outFile << personList[x].firstName << " " << personList[x].lastName << " " << personList[x].gender << " " << personList[x].yearOfBirth << " " << personList[x].yearOfDeath << " " << endl;
        }
    }
    outFile.close();
}

void PersonRepository::deletePerson(Person p)
{
    for(unsigned int x = 0; x < personList.size(); x++)
    {
        if(p.firstName == personList[x].firstName && p.lastName == personList[x].lastName)
        {
            personList.erase(personList.begin()+x);
        }
    }
    save();
}
