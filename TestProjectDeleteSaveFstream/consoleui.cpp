#include "consoleui.h"
#include <iomanip>
#include <vector>
#include <iostream>

using namespace std;

void display(vector<Person> personlist);
vector<Person> getFile();

ConsoleUI::ConsoleUI()
{
    personService = PersonService();
}

void ConsoleUI::start() {
    personService.getFile();
    bool checker = true;
    string inp;
    cout << "Welcome to the Database\n\n";
    cout << "The commands are: \n" << "- add\n" << "- display\n" << "- search\n" << "- delete\n" << "- quit\n";
    cout << endl;

    while(checker){

        cout << "Insert command: \n";

        cin >> inp;
        cout << endl;


        if(inp == "add") {
            Person p = Person();

            cout << "Insert: First name, Last name, Gender, Year of birth and Year of death.\n";
            cin >> p.firstName >> p.lastName >> p.gender >> p.yearOfBirth >> p.yearOfDeath;
            personService.add(p);
            cout << endl;

        }else if(inp == "display"){
            vector<Person> personlist = personService.getlist();
            display(personlist);

        }else if(inp == "search"){
            bool checker = true;
            char sCommand;
            string sInp;

            cout << "Search by:\n";
            cout << "- First Name(F)\n" << "- Last Name(L)\n" << "- Year Of Birth(B)\n" << "- Year Of Death(D)\n";

            while(checker){
                cin >> sCommand;
                sCommand = toupper(sCommand);

                if(sCommand == 'F' || sCommand == 'L' || sCommand == 'B' || sCommand == 'D'){
                    checker = false;
                }else{
                    cout << "*Invalid input*\n";
                }
            }


            switch(sCommand){
                case 'F':
                    cout << "Enter a first name to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                case 'L':
                    cout << "Enter a last name to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                case 'B':
                    cout << "Enter a date to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                case 'D':
                    cout << "Enter a date to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                default:
                    break;
                }

            cout << endl;

            display(personService.search(sCommand, sInp));

        }else if(inp == "delete"){
            Person p = Person();

            cout << "Insert: First name and last name.\n";
            cin >> p.firstName >> p.lastName;
            personService.deletePerson(p);
            cout << endl;
        }else if(inp == "quit"){
            checker = false;
        }else{
            cout << "*Invalid input*\n\n";
        }

    }
}

void display(vector<Person> personlist)
{
    cout << setw(10) << "First Name" << setw(15) << "Last Name" << setw(12) << "Gender";
    cout << setw(20) << "Year Of Birth" << setw(20) << "Year Of Death\n";
    for(int k = 0; k < 80 ; k++){
        cout << "-";
    }
    cout << endl;

    for(unsigned int i = 0; i < personlist.size(); i++){

        cout << personlist[i];
    }
}
