#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include <vector>
#include "person.h"

using namespace std;

class PersonRepository
{
private:
    vector<Person> personList;
    string toUppercase(string input);
public:
    PersonRepository();
    vector<Person> getlist();
    vector<Person> search(char sCommand, string sInp);
    void add(Person p);
    void save();
    void deletePerson(Person p);
    void getFile();
    vector<Person> sortList(vector<Person> personlist, char dCommand);
};

#endif // PERSONREPOSITORY_H
