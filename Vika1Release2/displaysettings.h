#ifndef DISPLAYSETTINGS_H
#define DISPLAYSETTINGS_H
#include <string>

using namespace std;


class DisplaySettings
{
public:
    DisplaySettings();
    DisplaySettings(char charOfSort, bool TypeOfSort);
    char sortByChar;
    bool sortTypeBool;
    void setSort();
    string sortBy;
    string sortType;
};

#endif // DISPLAYSETTINGS_H
