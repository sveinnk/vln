#ifndef PERSON_H
#define PERSON_H

#include <string>

using namespace std;

class Person
{
public:
    string name;
    string gender;
    string birthYear;
    string deathYear;
    Person();
};

#endif // PERSON_H
