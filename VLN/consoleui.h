#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <string>
#include <iostream>
#include "commands.h"

class ConsoleUI
{
public:
    ConsoleUI();
    void start();
private:
    Commands commands;
};

#endif // CONSOLEUI_H
