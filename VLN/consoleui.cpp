#include "consoleui.h"
#include "person.h"

using namespace std;

ConsoleUI::ConsoleUI()
{
    commands = Commands();
}


void ConsoleUI::start() {
    string inp;
    cout << "Welcome!!\nThe commands are:\t add" << endl;
    cin >> inp;

    if(inp == "add") {
        Person p = Person();
        cin >> p.name >> p.gender >> p.birthYear >> p.deathYear;
        cout << p.name << p.gender << p.birthYear << p.deathYear;
        commands.add(p);
        // Car c = Car();
        // cin >> c.color >> c.manufacturer >> c.model;

        // carService.add(c);
    }
}
