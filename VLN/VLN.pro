#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T12:49:40
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = VLN
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    person.cpp \
    commands.cpp

HEADERS += \
    consoleui.h \
    person.h \
    commands.h

OTHER_FILES += \
    outFile.txt
