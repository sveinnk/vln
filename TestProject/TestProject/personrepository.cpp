#include "personrepository.h"

PersonRepository::PersonRepository()
{
    personList = vector<Person>();
}

void PersonRepository::add(Person p)
{
    personList.push_back(p);
}

vector<Person> PersonRepository::getlist()
{
    return personList;
}

ostream& operator << (ostream& outs, const Person& person)
{
    string name = person.name;
    string gender = person.gender;
    int yearOfBirth = person.yearOfBirth;
    int yearOfDeath = person.yearOfDeath;

    outs << setw(20) << name << endl;
    outs << setw(8) << gender << endl;
    outs << setw(4) << yearOfBirth << endl;
    outs << setw(4) << yearOfDeath << endl;
    outs << endl;

    return outs;
}
