#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include <vector>
#include "person.h"

using namespace std;

class PersonRepository
{
private:
    vector<Person> personVector;
public:
    PersonRepository();
    void add(Person p);
    friend ostream& operator << (ostream& outs, const Person& person);
};

#endif // PERSONREPOSITORY_H
