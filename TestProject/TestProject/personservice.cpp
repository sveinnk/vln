#include "personservice.h"
#include "person.h"

PersonService::PersonService()
{
    personRepo = PersonRepository();
}

void PersonService::add(Person p) {
    personRepo.add(p);
}

list<Person> PersonService::getlist()
{
    return personRepo.getlist();
}
