#ifndef PERSON_H
#define PERSON_H

#include <string>

using namespace std;

class Person
{
public:
    string name;
    string gender;
    int yearOfBirth;
    int yearOfDeath;
    Person();
};

#endif // PERSON_H
