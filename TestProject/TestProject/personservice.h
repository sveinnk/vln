#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "person.h"
#include "personrepository.h"

class PersonService
{
private:
    PersonRepository personRepo;
public:
    PersonService();
    void add(Person p);
    vector<Person> getlist();
};

#endif // PERSONSERVICE_H
