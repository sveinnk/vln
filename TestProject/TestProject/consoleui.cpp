#include "consoleui.h"
#include <iomanip>
#include <vector>

using namespace std;

ConsoleUI::ConsoleUI()
{
    personService = PersonService();
}

void ConsoleUI::start() {
    bool checker = true;
    string inp;
    cout << "Welcome Database\n";
    cout << setw(20) << "The commands are: " << setw(7) << "add\n";
    cout << setw(27) << "display";

    while(checker){
        cin >> inp;

        if(inp == "add") {
            Person p = Person();
            cin >> p.name >> p.gender >> p.yearOfBirth >> p.yearOfDeath;
            personService.add(p);

        }else if(inp == "display"){
            vector<Person> personlist = personService.getlist();

            for(unsigned int i = 0; i < personlist.size(); i++){
                cout << personlist[i];
            }

        }else{
            checker = false;
        }

    }
}

