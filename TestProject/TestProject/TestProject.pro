#-------------------------------------------------
#
# Project created by QtCreator 2014-11-27T10:33:41
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = TestProject
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    person.cpp \
    personservice.cpp \
    personrepository.cpp

HEADERS += \
    consoleui.h \
    personservice.h \
    personrepository.h \
    person.h
