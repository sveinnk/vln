#include "consoleui.h"

using namespace std;

int main(int argc, char *argv[])
{
    ConsoleUI ui = ConsoleUI();

    ui.start();

    return 0;
}
