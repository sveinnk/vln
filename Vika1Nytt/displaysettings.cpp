#include "displaysettings.h"

DisplaySettings::DisplaySettings()
{
    sortByChar = 'F';
    sortTypeBool = true;
    setSort();
}

DisplaySettings::DisplaySettings(int numOfSort, bool typeOfSort)
{
    sortByChar = numOfSort;
    sortTypeBool = typeOfSort;
    setSort();
}

void DisplaySettings::setSort()
{
    switch(sortBy){
        case 'F':
            sortBy = "First Name";
            if(sortTypeBool){
                sortType = "Alphabetical order.";
            }else{
                sortType = "Reversed alphabetical order.";
            }
            break;
        case 'L':
            sortBy = "Last Name";
            if(sortTypeBool){
                sortType = "Alphabetical order.";
            }else{
                sortType = "Reversed alphabetical order.";
            }
            break;
        case 'G':
            sortBy = "Gender";
            if(sortTypeBool){
                sortType = "Males first.";
            }else{
                sortType = "Reversed alphabetical order.";
            }
            break;
        case 'B':
            sortBy = "Year Of Birth";
            if(sortTypeBool){
                sortType = "Increasing order.";
            }else{
                sortType = "Decreasing order.";
            }
            break;
        case 'D':
            sortBy = "Year Of Death";
            if(sortTypeBool){
                sortType = "Increasing order.";
            }else{
                sortType = "Decreasing order.";
            }
            break;
    }



}
