#ifndef DISPLAYSETTINGS_H
#define DISPLAYSETTINGS_H
#include <string>

class DisplaySettings
{
public:
    DisplaySettings();
    DisplaySettings(char sortByNum, bool sortType);
    char sortByChar;
    bool sortTypeBool;
    void setSort();
    string sortBy;
    string sortType;
};

#endif // DISPLAYSETTINGS_H
