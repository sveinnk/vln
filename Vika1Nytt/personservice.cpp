#include "personservice.h"
#include "person.h"

PersonService::PersonService()
{
    personRepo = PersonRepository();
}

void PersonService::add(Person p) {
    personRepo.add(p);
}

vector<Person> PersonService::getlist()
{
    return personRepo.getlist();
}

vector<Person> PersonService::search(char sCommand, string sInp)
{
    return personRepo.search(sCommand, sInp);
}

void PersonService::deletePerson(Person p)
{
    personRepo.deletePerson(p);
}

void PersonService::getFile()
{
    personRepo.getFile();
}

/*void PersonService::sortList(char dCommand)
{
    personRepo.sortList(dCommand);
}*/
