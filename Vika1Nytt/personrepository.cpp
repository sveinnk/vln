#include "personrepository.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cctype>


PersonRepository::PersonRepository()
{
    personList = vector<Person>();
}

void PersonRepository::add(Person p)
{
    personList.push_back(p);
    save();
}

vector<Person> PersonRepository::getlist()
{
    return personList;
}

vector<Person> PersonRepository::search(char sCommand, string sInp)
{
    vector<Person> searchList;

    sInp = toUppercase(sInp);

    if(personList.size() > 0){
        switch(sCommand){
            case 'F':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].firstName)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            case 'L':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].lastName)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
            break;
            case 'G':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].gender)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            case 'B':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].yearOfBirth)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            case 'D':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].yearOfDeath)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            default:
                break;
        }
    }
    return searchList;
}

void PersonRepository::getFile()
{
    string word;
    Person p;
    int x = 0;
    ifstream inFile ("gagnagrunnur.txt");
    if (inFile.is_open())
    {
        while (getline(inFile,word,' '))
        {
            if(x==0){
                if(word[0] == '\n')
                {
                    word.erase(0,1);
                }
                p.firstName = word;
                x++;
            }
            else if(x==1){
                p.lastName = word;
                x++;
            }
            else if(x==2){
                p.gender = word;
                x++;
            }
            else if(x==3){
                p.yearOfBirth = word;
                x++;
            }
            else if(x==4){
                p.yearOfDeath = word;
                personList.push_back(p);
                x=0;
            }
        }
        inFile.close();
    }
}

void PersonRepository::save()
{
    ofstream outFile ("gagnagrunnur.txt");
    if(outFile.is_open()) {
        for(unsigned int x = 0; x < personList.size(); x++)
        {
            outFile << personList[x].firstName << " " << personList[x].lastName << " " << personList[x].gender << " " << personList[x].yearOfBirth << " " << personList[x].yearOfDeath << " " << endl;
        }
    }
    outFile.close();
}

void PersonRepository::deletePerson(Person p)
{

    for(unsigned int x = 0; x < personList.size(); x++)
    {
        if(toUppercase(p.firstName) == toUppercase(personList[x].firstName) && toUppercase(p.lastName) == toUppercase(personList[x].lastName))
        {
            personList.erase(personList.begin()+x);
        }
    }
    save();
}

string PersonRepository::toUppercase(string input)
{
    for(unsigned int i = 0; i < input.length(); i++){
        input[i] = toupper(input[i]);
    }

    return input;
}

bool PersonRepository::sortByFirstName(const Person &lhs, const Person &rhs)
{
    return lhs.firstName < rhs.firstName;
}

bool PersonRepository::sortByLastName(const Person &lhs, const Person &rhs)
{
    return lhs.lastName < rhs.lastName;
}

bool PersonRepository::sortByGender(const Person &lhs, const Person &rhs)
{
    return lhs.gender < rhs.gender;
}

bool PersonRepository::sortByBirthYear(const Person &lhs, const Person &rhs)
{
    return lhs.yearOfBirth < rhs.yearOfBirth;
}

bool PersonRepository::sortByDeathYear(const Person &lhs, const Person &rhs)
{
    return lhs.yearOfDeath < rhs.yearOfDeath;
}


void PersonRepository::sortList(char dCommand)
{

    switch(dCommand){
        case 'F':
            sort(personList.begin(), personList.end(), sortByFirstName);
            break;
        case 'L':
            sort(personList.begin(), personList.end(), sortByLastName);
            break;
        case 'G':
            sort(personList.begin(), personList.end(), sortByGender);
            break;
        case 'B':
            sort(personList.begin(), personList.end(), sortByBirthYear);
            break;
        case 'D':
            sort(personList.begin(), personList.end(), sortByDeathYear);
            break;
    }
}

