#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "person.h"
#include "personrepository.h"

class PersonService
{
private:
    PersonRepository personRepo;
public:
    PersonService();
    void add(Person p);
    vector<Person> getlist();
    vector<Person> search(char sCommand, string sInp);
    void deletePerson(Person p);
    void getFile();
    void sortList(char dCommand);
};

#endif // PERSONSERVICE_H
