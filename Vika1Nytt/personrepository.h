#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include <vector>
#include "person.h"

using namespace std;

class PersonRepository
{
private:
    vector<Person> personList;
    bool sortByFirstName(const Person &lhs, const Person &rhs);
    bool sortByLastName(const Person &lhs, const Person &rhs);
    bool sortByGender(const Person &lhs, const Person &rhs);
    bool sortByBirthYear(const Person &lhs, const Person &rhs);
    bool sortByDeathYear(const Person &lhs, const Person &rhs);
public:
    PersonRepository();

    vector<Person> getlist();
    vector<Person> search(char sCommand, string sInp);
    string toUppercase(string input);
    void add(Person p);
    void save();
    void deletePerson(Person p);
    void getFile();
    void sortList(char dCommand);
};

#endif // PERSONREPOSITORY_H
