#include "consoleui.h"
#include <iomanip>
#include <vector>

using namespace std;

ConsoleUI::ConsoleUI()
{
    personService = PersonService();
}

void ConsoleUI::start() {
    personService.getFile();
    bool checker = true;
    string inp;
    cout << "Welcome to the Database\n\n";
    cout << "The commands are: \n" << "- add\n" << "- display\n" << "- search\n" << "- delete\n" << "- quit\n";
    cout << endl;

    while(checker){

        cout << "Insert command: \n";

        cin >> inp;
        cout << endl;


        if(inp == "add") {
            Person p = Person();

            cout << "First name: "; cin >> p.firstName;
            cout << "Last name: "; cin >> p.lastName;
            cout << "Gender(M/F): "; cin >> p.gender;
            cout << "Year of birth: "; cin >> p.yearOfBirth;
            cout << "Year of death: "; cin >> p.yearOfDeath;

            personService.add(p);
            cout << endl;

        }else if(inp == "display"){
            char sCommand;
            cout << "Sort by:\n";
            cout << "- First Name(F)\n" << "- Last Name(L)\n" << "- Gender(G)\n" << "- Year Of Birth(B)\n" << "- Year Of Death(D)\n" << endl;
            cin >> sCommand;
            vector<Person> personlist = personService.getlist(sCommand);

            display(personlist);

        }else if(inp == "search"){
            bool checker = true;
            char sCommand;
            string sInp;

            cout << "Search by:\n";
            cout << "- First Name(F)\n" << "- Last Name(L)\n" << "- Gender(G)\n" << "- Year Of Birth(B)\n" << "- Year Of Death(D)\n";

            while(checker){
                cin >> sCommand;
                sCommand = toupper(sCommand);

                if(sCommand == 'F' || sCommand == 'L' || sCommand == 'G' || sCommand == 'B' || sCommand == 'D'){
                    checker = false;
                }else{
                    cout << "*Invalid input*\n";
                }
            }


            switch(sCommand){
                case 'F':
                    cout << "Enter a first name to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                case 'L':
                    cout << "Enter a last name to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                case 'G':
                    cout << "Enter a gender to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                case 'B':
                    cout << "Enter a date to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                case 'D':
                    cout << "Enter a date to search for: ";
                    cin >> sInp;
                    checker = false;
                    break;
                default:
                    break;
                }

            cout << endl;

            display(personService.search(sCommand, sInp));
        }else if(inp == "delete"){
            Person p = Person();
            cout << "Insert: First name and last name.\n";
            cin >> p.firstName >> p.lastName;
            personService.deletePerson(p);
            cout << endl;
        }else if(inp == "quit"){
            checker = false;
        }else{
            cout << "*Invalid input*\n\n";
        }

    }
}

void ConsoleUI::display(vector<Person> personlist)
{
    char dCommand;
    char dInp;
    bool checker = true;
    /*vector<Person> sortedList;

    cout << "Display based on:\n ";
    cout << "- First Name(F)\n" << "- Last Name(L)\n" << "- Gender(G)\n" << "- Year Of Birth(B)\n" << "- Year Of Death(D)\n";

    while(checker){
        cin >> dCommand;
        dCommand = toupper(dCommand);

        checker = check5options(dCommand, 'F', 'L', 'G', 'B', 'D');

    }

    checker = true;

    if(dCommand == 'F' || dCommand == 'L'){
        cout << "Alphabetical(A) or Reverse Alphabetical(R) order?\n";
        while(checker){
            cin >> dInp;
            checker = check2options(dInp, 'A', 'R');
        }
    }else if(dCommand == 'G'){
        cout << "Males first(M) or Females first(F)?\n";
        while(checker){
            cin >> dInp;
            checker = check2options(dInp, 'M', 'F');
        }
    }else{
        cout << "Increasing(I) or Decreasing(D) order?\n";
        while(checker){
            cin >> dInp;
            checker = check2options(dInp, 'I', 'D');
        }
    }



    personlist = personService.sort(dCommand, dInp);
*/

    cout << setw(10) << "First Name" << setw(15) << "Last Name" << setw(12) << "Gender";
    cout << setw(20) << "Year Of Birth" << setw(20) << "Year Of Death\n";
    for(int k = 0; k < 80 ; k++){
        cout << "-";
    }
    cout << endl;

    for(unsigned int i = 0; i < personlist.size(); i++){
        cout << personlist[i];
    }

}

bool ConsoleUI::check2options(char input, char option1, char option2)
{
    bool checker = false;

    if(input == option1 || input == option2){
        checker = true;
    }else{
        cout << "*Invalid input*";
    }

    return checker;
}

bool ConsoleUI::check5options(char input, char option1, char option2, char option3, char option4, char option5)
{
    bool checker = false;

    if(input == option1 || input == option2 || input == option3 || input == option4 || input == option5){
        checker = true;
    }else{
        cout << "*Invalid input*";
    }

    return checker;
}

