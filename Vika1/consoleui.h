#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <string>
#include <iostream>
#include "personservice.h"

class ConsoleUI
{
public:
    ConsoleUI();
    void start();
private:
    PersonService personService;
    void display(vector<Person> personlist);
    bool check2options(char input, char option1, char option2);
    bool check5options(char input, char option1, char option2, char option3, char option4, char option5);
};

#endif // CONSOLEUI_H
