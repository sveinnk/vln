#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include <vector>
#include "person.h"

using namespace std;

class PersonRepository
{
private:
    vector<Person> personList;
public:
    PersonRepository();
    void add(Person p);
    vector<Person> getlist(char sCommand);
    vector<Person> search(char sCommand, string sInp);
    //vector<Person> sort(char dCommand, char dInp);
    //vector<Person> quickSort(vector<Person> list, int left, int right, char incOrDec, string cat);
    string toUppercase(string input);
    void save();
    void deletePerson(Person p);
    void getFile();
    void personSort(char c);
};

#endif // PERSONREPOSITORY_H
