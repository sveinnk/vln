#include "person.h"
#include <iomanip>
#include <iostream>


Person::Person()
{
    firstName = "";
    lastName = "";
    gender = "";
    yearOfBirth = "";
    yearOfDeath = "";
}

ostream& operator << (ostream& outs, const Person& person)
{
    string firstName = person.firstName;
    string lastName = person.lastName;
    string gender = person.gender;
    string yearOfBirth = person.yearOfBirth;
    string yearOfDeath = person.yearOfDeath;

    outs << setw(10) << firstName << setw(15) << lastName << setw(12) << gender;
    outs << setw(20) << yearOfBirth << setw(20) << yearOfDeath << endl << endl;

    return outs;
}

