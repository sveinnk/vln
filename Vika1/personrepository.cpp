#include "personrepository.h"
#include <iostream>
#include <fstream>
#include <algorithm>


PersonRepository::PersonRepository()
{
    personList = vector<Person>();
}

void PersonRepository::add(Person p)
{
    personList.push_back(p);
    save();
}

vector<Person> PersonRepository::getlist(char sCommand)
{
    personSort(sCommand);
    return personList;
}

vector<Person> PersonRepository::search(char sCommand, string sInp)
{
    vector<Person> searchList;

    sInp = toUppercase(sInp);

    if(personList.size() > 0){
        switch(sCommand){
            case 'F':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].firstName)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            case 'L':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].lastName)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
            break;
            case 'G':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].gender)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            case 'B':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].yearOfBirth)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            case 'D':
                for(unsigned int i = 0; i < personList.size(); i++){
                    if(sInp.compare(toUppercase(personList[i].yearOfDeath)) == 0){
                        searchList.push_back(personList[i]);
                    }
                }
                break;
            default:
                break;
        }
    }
    return searchList;
}

void PersonRepository::getFile()
{
    string word;
    Person p;
    int x = 0;
    ifstream inFile ("gagnagrunnur.txt");
    if (inFile.is_open())
    {
        while (getline(inFile,word,' '))
        {
            if(x==0){
                if(word[0] == '\n')
                {
                    word.erase(0,1);
                }
                p.firstName = word;
                x++;
            }
            else if(x==1){
                p.lastName = word;
                x++;
            }
            else if(x==2){
                p.gender = word;
                x++;
            }
            else if(x==3){
                p.yearOfBirth = word;
                x++;
            }
            else if(x==4){
                p.yearOfDeath = word;
                personList.push_back(p);
                x=0;
            }
        }
        inFile.close();
    }
}

void PersonRepository::save()
{
    ofstream outFile ("gagnagrunnur.txt");
    if(outFile.is_open()) {
        for(unsigned int x = 0; x < personList.size(); x++)
        {
            outFile << personList[x].firstName << " " << personList[x].lastName << " " << personList[x].gender << " " << personList[x].yearOfBirth << " " << personList[x].yearOfDeath << " " << endl;
        }
    }
    outFile.close();
}

void PersonRepository::deletePerson(Person p)
{
    for(unsigned int x = 0; x < personList.size(); x++)
    {
        if(p.firstName == personList[x].firstName && p.lastName == personList[x].lastName)
        {
            personList.erase(personList.begin()+x);
        }
    }
    save();
}

/*vector<Person> PersonRepository::sort(char dCommand, char dInp)
{
    vector<Person> sortedList = personList;

    if(dInp == 'A'){
        for (int i = 1; i < sortedList.size(); i++){
                string x, j;
                j = i;
                x = sortedList[i];
                while (j > 0 && sortedList[j - 1] > x){
                    sortedList[j] = sortedList[j - 1];
                    j--;
                }
            sortedList[j] = x;
    }





}*/


string PersonRepository::toUppercase(string input)
{
    for(unsigned int i = 0; i < input.length(); i++){
        input[i] = toupper(input[i]);
    }

    return input;
}

/*vector<Person> PersonRepository::quickSort(vector<Person> list, int left, int right, bool increasing, string cat)
{
    vector<Person> sortedList = list;
    int i = left, j = right;
    string tmp;

    if(increasing){
        string pivot = sortedList.cat[(left + right) / 2];
            while (i <= j) {
                while (sortedList.cat[i] < pivot)
                    i++;
                while (sortedList.cat[j] > pivot)
                    j--;
                if (i <= j) {
                    tmp = sortedList[i];
                    sortedList[i] = sortedList[j];
                    sortedList[j] = tmp;
                    i++;
                    j--;
                }
            }

        if (left < j)
            quickSort(sortedList, left, j, incOrDec, cat);
        if (i < right)
            quickSort(sortedList, i, right, incOrDec, cat);
    }else{
        string pivot = sortedList.cat[(left + right) / 2];
            while (i => j){
                while (sortedList.cat[i] > pivot)
                    i++;
                while (sortedList.cat[j] < pivot)
                    j--;
                if (i => j){
                    tmp = sortedList[i];
                    sortedList[i] = sortedList[j];
                    sortedList[j] = tmp;
                    i++;
                    j--;
                }
            }

        if (left > j)
            quickSort(sortedList, left, j, incOrDec, cat);
        if (i < right)
            quickSort(sortedList, i, right, incOrDec, cat);
    }

    return sortedList;

}*/



bool sortByFirstName(const Person &lhs, const Person &rhs) { return lhs.firstName < rhs.firstName; }

bool sortByLastName(const Person &lhs, const Person &rhs) { return lhs.lastName < rhs.lastName; }

bool sortByGender(const Person &lhs, const Person &rhs) { return lhs.gender < rhs.gender; }

bool sortByBirthYear(const Person &lhs, const Person &rhs) { return lhs.yearOfBirth < rhs.yearOfBirth; }

bool sortByDeathYear(const Person &lhs, const Person &rhs){ return lhs.yearOfDeath < rhs.yearOfDeath; }


void PersonRepository::personSort(char c)
{
    if(c == 'F' || c  == 'f')
    {
        sort(personList.begin(), personList.end(), sortByFirstName);
    }
    else if(c == 'L' || c == 'l')
    {
        sort(personList.begin(), personList.end(), sortByLastName);
    }
    else if(c == 'G' || c == 'g')
    {
        sort(personList.begin(), personList.end(), sortByGender);
    }
    else if(c == 'B' || c == 'b')
    {
        sort(personList.begin(), personList.end(), sortByBirthYear);
    }
    else if(c == 'D' || c == 'd')
    {
        sort(personList.begin(), personList.end(), sortByDeathYear);
    }
}
