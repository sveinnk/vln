#ifndef PERSON_H
#define PERSON_H

#include <string>

using namespace std;

class Person
{
public:
    string firstName;
    string lastName;
    string gender;
    string yearOfBirth;
    string yearOfDeath;
    Person();
    friend ostream& operator << (ostream& outs, const Person& person);

};

#endif // PERSON_H
