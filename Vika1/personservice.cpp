#include "personservice.h"
#include "person.h"

PersonService::PersonService()
{
    personRepo = PersonRepository();
}

void PersonService::add(Person p) {
    personRepo.add(p);
}

vector<Person> PersonService::getlist(char sCommand)
{
    return personRepo.getlist(sCommand);
}

vector<Person> PersonService::search(char sCommand, string sInp)
{
    return personRepo.search(sCommand, sInp);
}

void PersonService::deletePerson(Person p)
{
    personRepo.deletePerson(p);
}

void PersonService::getFile()
{
    personRepo.getFile();
}

/*vector<Person> PersonService::sort(char dCommand, char dInp)
{
    return personRepo.sort(dCommand, dInp);
}*/
