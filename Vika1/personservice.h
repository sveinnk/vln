#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "person.h"
#include "personrepository.h"

class PersonService
{
private:
    PersonRepository personRepo;
public:
    PersonService();
    void add(Person p);
    vector<Person> getlist(char sCommand);
    vector<Person> search(char sCommand, string sInp);
    void deletePerson(Person p);
    void getFile();
    //vector<Person> sort(char dCommand, char dInp);
};

#endif // PERSONSERVICE_H
